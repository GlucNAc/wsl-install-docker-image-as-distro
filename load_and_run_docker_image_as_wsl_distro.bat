@ECHO OFF
REM set wsl_distro_path=C:\wsldistros
REM set wsl_distro_name=wsl-debian
REM set docker_image_repo=glucnac
REM set docker_image_name=wsl-debian_11.2-slim
REM set docker_image_tag=1.0

REM Set variables from user input
set wsl_distro_path=%1
set wsl_distro_name=%2
set docker_image_repo=%3
set docker_image_name=%4
set docker_image_tag=%5

REM Set variables as mandatory for the script to run
if "%wsl_distro_path%"=="" (
    echo "wsl_distro_path is not set"
    exit /b 1
)
if "%wsl_distro_name%"=="" (
    echo "wsl_distro_name is not set"
    exit /b 1
)
if "%docker_image_repo%"=="" (
    echo "docker_image_repo is not set"
    exit /b 1
)
if "%docker_image_name%"=="" (
    echo "docker_image_name is not set"
    exit /b 1
)
if "%docker_image_tag%"=="" (
    echo "docker_image_tag is not set"
    exit /b 1
)

REM Generate a docker image archive that can be imported as a WSL2 distro
docker run --name %wsl_distro_name% %docker_image_repo%/%docker_image_name%:%docker_image_tag%
mkdir %wsl_distro_path%
docker export -o %wsl_distro_path%\%docker_image_name%.tar.gz %wsl_distro_name%

REM Remove the docker container and image as they are no longer needed
docker rm %wsl_distro_name%
wsl --unregister %docker_image_name%

REM Import the docker image archive as a WSL2 distro and run it
mkdir %wsl_distro_path%\%wsl_distro_name%
wsl --import %wsl_distro_name% %wsl_distro_path%\%wsl_distro_name% %wsl_distro_path%\%docker_image_name%.tar.gz --version 2
wsl -d %wsl_distro_name%